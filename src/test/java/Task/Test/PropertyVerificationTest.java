package Task.Test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import Task.PageObjects.HomePage;
import Task.PageObjects.PropertyForSalePage;
import Task.Utilities.BaseClass;

public class PropertyVerificationTest extends BaseClass{
	/*WebDriver driver;
	
	public PropertyVerificationTest(WebDriver driver) {
		this.driver=driver;
	}*/
	
	@Test(priority=0,enabled=true)
	public void validateMyLocationProperty() throws InterruptedException {
		
		HomePage homePage = PageFactory.initElements(getDriver(), HomePage.class);
		//HomePage homePage = new HomePage(this.driver);
		PropertyForSalePage propertyForSale = PageFactory.initElements(driver, PropertyForSalePage.class);
		//PropertyForSalePage propertyForSale = new PropertyForSalePage(this.driver);
		homePage.clickonAgree();
		Thread.sleep(500); 
		homePage.clickonQuery();
		homePage.clickOnMyLocation();
		homePage.clickonForSale();
		Thread.sleep(1000);
		driver.navigate().refresh();
		Thread.sleep(500);
		propertyForSale.validateMyLocationURL("My+Location");
		propertyForSale.validatePropertytitle("BT2", "BT7");
	
	}
	
	@Test(priority=1,enabled=true)
	public void validatePropertyCode() throws InterruptedException {
		
		HomePage homePage = PageFactory.initElements(getDriver(), HomePage.class);
		PropertyForSalePage propertyForSale = PageFactory.initElements(driver, PropertyForSalePage.class);
		homePage.clickonAgree();
		Thread.sleep(500);
		homePage.sendPostCode("BT6");
		homePage.clickonForRent();
		Thread.sleep(1000);
		driver.navigate().refresh();
		Thread.sleep(500);
		propertyForSale.validatePropertytitle("BT6");
		
	}

}
