package Task.Utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

public class BaseClass {
	public static WebDriver driver;

	@BeforeMethod
	public void intialise() {
		driver=BrowserFactory.StartBrowser("chrome");
	}

	@AfterMethod
	public void quit() {
		BrowserFactory.browserQuit();
	}

	public WebDriver getDriver() {
		return driver;
	}

	

}