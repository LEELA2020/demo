package Task.Utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CommonClass {
	WebDriver driver;
	
	public CommonClass(WebDriver driver) {
		this.driver=driver;
	}
	
	public void click(WebElement ele) {
		ele.click();
	}
	
	public void enterText(WebElement ele, String str) {
		ele.sendKeys(str);
	}

}
