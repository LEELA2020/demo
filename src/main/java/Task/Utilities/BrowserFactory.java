package Task.Utilities;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class BrowserFactory {
	static WebDriver driver;
	

	public static WebDriver StartBrowser(String browserName) {
		try {
			if (browserName.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "C:\\Users\\Suresh\\Downloads\\chromedriver_win32\\chromedriver.exe");
				ChromeOptions options = new ChromeOptions();
				HashMap<String,Integer> contentSettings= new HashMap<String,Integer>();
				HashMap<String,Object> profile= new HashMap<String,Object>();
				HashMap<String,Object> pref= new HashMap<String,Object>();
				contentSettings.put("geolocation", 1);
				profile.put("managed_default_content_settings", contentSettings);
				pref.put("profile", profile);
				options.setExperimentalOption("pref", pref);
				driver = new ChromeDriver();
				
				driver.manage().window().maximize();

				/*driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);*/

				driver.get("https://www.propertypal.com");
				
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			}else if(browserName.equalsIgnoreCase("firefox")) {
				driver = new FirefoxDriver();
			}
		
		}catch(Exception e) {
			e.printStackTrace();
		}
		return driver;
	}
	
	public static void browserQuit() {
		driver.quit();
	}

	/*@Test
	public void testingTest() {
		StartBrowser("chrome");
	}*/
}
