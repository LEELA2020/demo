package Task.PageObjects;



import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.testng.Assert;

public class PropertyForSalePage {
	WebDriver driver;
	
	public PropertyForSalePage(WebDriver driver) {
		this.driver=driver;
	}
	
	@FindBys({@FindBy(xpath="//div[@class='propbox-details']/h2")})
	private List<WebElement> PostCode;
	
	
	@FindBy(xpath="//h1[@class='noresults-heading']")
	private WebElement noPropertyMsg;
	
	
	/*public void validatePropertytitle(String str1, String str2) {
		try {
			if (PostCode.size()>0) {
				for (int i = 0; i < PostCode.size(); i++) {
					if (PostCode.get(i).getText().contains(str1)||PostCode.get(i).getText().contains(str2)) {
						System.out.println("verified successfully");
					}else {
						System.out.println("verification failed");
					}
				}
			}
		} catch (Exception e) {
			if (noPropertyMsg.isDisplayed()) {
				System.out.println("Verify No Property Message"+noPropertyMsg.getText());
			}
		}
		
	}*/
	
	public void validatePropertytitle(String... strArr) {
		try {
			if (PostCode.size()>0) {
				for (int i = 0; i < PostCode.size(); i++) {
					boolean isMatched = false;
					String btValue = null;
					for(int j=0; j<strArr.length; j++) {
						btValue = strArr[j];
						if (PostCode.get(i).getText().contains(btValue)) {
							isMatched = true;
						}
					}
					Assert.assertTrue(isMatched, btValue+" is not matched with "+PostCode.get(i).getText());
					
				}
			}
		} catch (Exception e) {
			if (noPropertyMsg.isDisplayed()) {
				System.out.println("Verify No Property Message"+noPropertyMsg.getText());
			}
		}
		
	}
	
	public void validateMyLocationURL(String strURL) {
		try {
			String myLocUrl= driver.getCurrentUrl();
			if (myLocUrl.contains(strURL)) {
				System.out.println("URL Contains Mylocation which is expected");
			}else {
				System.out.println("URL not Contains Mylocation which is not expected");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void validateNotMyLocationURL(String strURL) {
		try {
			String myLocUrl= driver.getCurrentUrl();
			if (!myLocUrl.contains(strURL)) {
				System.out.println("URL not Contains Mylocation which is expected");
			}else {
				System.out.println("URL Contains Mylocation which is not expected");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
