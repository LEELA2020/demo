package Task.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import Task.Utilities.CommonClass;

public class HomePage {
	WebDriver driver;
	CommonClass cc;
	
	public HomePage(WebDriver driver) {
		this.driver=driver;
		cc=new CommonClass(driver);
		//PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//div[@class='search-ctrl query  suggestions-container']//input")
	private WebElement search_TxtBox;
	
	@FindBy(xpath="//a/strong[text()='My Location']")
	private WebElement suggestion_Location;
	
	@FindBy(xpath="//button[text()='AGREE']")
	private WebElement agree_Btn;
	
	@FindBy(xpath="//button[text()='For Sale']")
	private WebElement forSale_Btn;
	
	@FindBy(xpath="//button[text()='For Rent']")
	private WebElement forRent_Btn;
	

	public void clickonQuery() {
		cc.click(search_TxtBox);
	}
	
	public void clickOnMyLocation() {
		cc.click(suggestion_Location);
	}
	
	public void clickonAgree() {
		cc.click(agree_Btn);
	}
	
	public void clickonForSale() {
		cc.click(forSale_Btn);
	}
	
	public void clickonForRent() {
		cc.click(forRent_Btn);
	}
	
	public void sendPostCode(String strPostCode) {
		cc.enterText(search_TxtBox, strPostCode);
	}
}
